import random

data = {
    'hand_dealt': {
        'player': [],
        'dealer': []
    },

    'deck': {
        'clubs': range(1, 14),
        'diamonds': range(1, 14),
        'hearts': range(1, 14),
        'spades': range(1, 14)
    },

    'game_state': True
}


def gametime():
    '''
    Initializes the game
    '''
    initial_counter = 0
    while initial_counter != 4:
        if initial_counter % 2 == 0:
            deal_turn = 'player'
        else:
            deal_turn = 'dealer'

        draw_card(deal_turn)
        initial_counter += 1

    # Continue here


def draw_card(turn):
    suite = random.choice(list(data['deck']))
    if data['deck'][suite]:
        random_num = random.choice(data['deck'][suite])
        card_index = data['deck'][suite].index(random_num)
        card = list(data['deck'][suite]).pop(card_index)
        data['hand_dealt'][turn].append(f'{str(card)} of {suite.title()}')
    else:
        draw_card(turn)


# while data['game_state']:
#     gametime()

gametime()
print(data['hand_dealt'])
