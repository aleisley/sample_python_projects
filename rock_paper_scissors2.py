import random


def play_game():
    data_structure = {
        'counter': {
            'player': 0,
            'ai': 0,
            'tie': 0
        },

        'choices': {
            1: 'Rock',
            2: 'Paper',
            3: 'Scissors'
        }
    }

    while True:
        play_round(data_structure)
        if check_for_game_winner(data_structure):
            break


def play_round(data):
    print('Please choose one of the following: ')
    for key, value in data['choices'].items():
        print(f'{key}: {value}')

    choice_of_obj = int(input('Your choice: '))

    if choice_of_obj in range(1, 4):
        winner = round_winner(choice_of_obj, data)
        data['counter'][winner] += 1
        display_round_winner(winner, data)


def round_winner(choice, data):
    ai_chosen = random.randint(1, 3)
    print(f"AI chose {data['choices'][ai_chosen]}")

    if ai_chosen % 3 == (choice + 1) % 3:
        return 'ai'
    elif ai_chosen == choice:
        return 'tie'
    else:
        return 'player'


def display_round_winner(winner, data):
    if winner == 'tie':
        print('This round is tied!')
    else:
        print(f'The winner this round is {winner.upper()}')

    print('Current points are as follows: ')

    for key, value in data['counter'].items():
        print(f"{key}: {value}")


def check_for_game_winner(data):
    for key, value in data['counter'].items():
        if key != 'tie' and value == 3:
            print(f'And the winner is: {key.title()}')
            return True


play_game()
