import random


def round_winner(choice):
    ai_chosen = str(random.randint(1, 3))
    print(f'AI chose {ai_chosen}')
    if ai_chosen % 3 == (choice + 1) % 3:
        return 'ai'
    elif ai_chosen == choice:
        return 'tie'
    else:
        return 'player'


def display_round_winner(winner):
    if winner == 'tie':
        print('This round is tied!')
    else:
        print(f'The winner this round is the {winner.upper()}')

    print(f'''
    Current points as follows:
    Player: {counter['player']}
    AI: {counter['ai']}
    Rounds Tied: {counter['tie']}
    ''')


def check_for_game_winner():
    global game_ongoing
    for key, value in counter.items():
        if value == 2:
            print(f'{key.upper()} wins the game!')
            game_ongoing = False


def play_games():
    global counter
    message = '''
    Please choose one of the following:
    1: Rock
    2: Paper
    3: Scissors
    '''

    print(message)

    choice_of_obj = input('What will it be: ')
    if choice_of_obj in ['1', '2', '3']:
        winner = round_winner(choice_of_obj)
        counter[winner] += 1
        display_round_winner(winner)
        check_for_game_winner()
    else:
        print('Out of bounds')


counter = {
    'player': 0,
    'ai': 0,
    'tie': 0
}

game_ongoing = True

while game_ongoing:
    initializer()
